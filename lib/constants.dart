import 'package:flutter/material.dart';

const kBottomContainerColor = Color(0xFFEB1555);
const kBottomContainerHeight = 60.0;
const kActiveCardColor = Color(0xFF1D1E33);
const kInactiveActiveCardColor = Color(0xFF111328);
const kMinSliderValue = 120.0;
const kMaxSliderValue = 220.0;

const kLabelStyle = TextStyle(color: Color(0xFF8D8E98), fontSize: 18.0);

const kLargeNumberLabelStyle =
    TextStyle(fontSize: 50.0, fontWeight: FontWeight.w900);

const kLargeTextLabelStyle =
    TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);

const kResultTextLabelStyle = TextStyle(
    fontSize: 22.0, color: Color(0XFF24D876), fontWeight: FontWeight.bold);

const kBMITextLabelStyle =
    TextStyle(fontSize: 100.0, fontWeight: FontWeight.bold);

const kBodyTextLabelStyle = TextStyle(fontSize: 22.0);
