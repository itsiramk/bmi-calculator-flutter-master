# BMI Calculator 💪

This is a BMI calculator designed in Flutter using its latest design principles.

We are taking user's input to enter age and weight and show if a person is Overweight, Normal or Underweight.

We have also used Navigation to and fro across the screens.

